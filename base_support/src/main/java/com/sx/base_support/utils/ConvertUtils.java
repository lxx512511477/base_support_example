package com.sx.base_support.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Base64;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * @Describe 转换相关工具类
 */

public class ConvertUtils {

    /**
     * KB 与 Byte 的倍数
     */
    public static final int KB = 1024;
    /**
     * MB 与 Byte 的倍数
     */
    public static final int MB = 1048576;
    /**
     * GB 与 Byte 的倍数
     */
    public static final int GB = 1073741824;

    private ConvertUtils() {
        throw new UnsupportedOperationException("u can't instantiate me...");
    }

    public static <T> T base64Str2Object(String productBase64) {
        T device = null;
        if (productBase64 == null) {
            return null;
        }
        // 读取字节
        byte[] base64 = Base64.decode(productBase64.getBytes(), Base64.DEFAULT);

        // 封装到字节流
        ByteArrayInputStream bais = new ByteArrayInputStream(base64);
        try {
            // 再次封装
            ObjectInputStream bis = new ObjectInputStream(bais);
            // 读取对象
            device = (T) bis.readObject();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return device;
    }

    public static <T> String object2Base64Str(T object) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {   //Device为自定义类
            // 创建对象输出流，并封装字节流
            ObjectOutputStream oos = new ObjectOutputStream(baos);
            // 将对象写入字节流
            oos.writeObject(object);
            // 将字节流编码成base64的字符串
            return new String(Base64.encode(baos
                    .toByteArray(), Base64.DEFAULT));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 数字格式转换，超过 9999 用 “1万”
     * ⦁	数字不展示超过五位数，超过9999则显示1W（W大写），超过11000则显示1.1W、1.2W，超过99999则显示10W、11W
     *
     * @param number
     * @return
     */
    public static String numberConvert(int number) {
        if (number > 9999) {
            if (number >= 100000) {
                return number / 10000 + "W";
            } else {
                return number / 10000 + "." + ((number-number/10000*10000) / 1000) + "W";
            }
        }
        return String.valueOf(number);
    }

    /**
     * 字节数转合适内存大小，单位B K M G
     * <p>保留3位小数</p>
     *
     * @param byteNum 字节数
     * @return 合适内存大小
     */
    @SuppressLint("DefaultLocale")
    public static String byte2FitMemorySizeUnit(long byteNum) {
        if (byteNum < 0) {
            return "shouldn't be less than zero!";
        } else if (byteNum < KB) {
            return String.format("%.1fB", byteNum + 0.05);
        } else if (byteNum < MB) {
            return String.format("%.1fK", byteNum / KB + 0.05);
        } else if (byteNum < GB) {
            return String.format("%.1fM", byteNum / MB + 0.05);
        } else {
            return String.format("%.1fG", byteNum / GB + 0.05);
        }
    }
}
