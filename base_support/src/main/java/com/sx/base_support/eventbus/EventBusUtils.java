package com.sx.base_support.eventbus;

import org.greenrobot.eventbus.EventBus;

/**
 * eventbus封装类
 * 使用说明：1、在接收eventbus事件的类中 isRegisteredEventBus return true
 * protected boolean isRegisteredEventBus() {
 * return true;
 * }
 * 2、重写handleEvent处理事件
 *
 * @Subscribe(threadMode = ThreadMode.MAIN)
 * public void onReceiveEvent(BaseEvent baseEvent) {
 * }
 */
public class EventBusUtils {
    private EventBusUtils() {
    }

    /**
     * 注册 EventBus
     *
     * @param subscriber
     */
    public static void register(Object subscriber) {
        EventBus eventBus = EventBus.getDefault();
        if (!eventBus.isRegistered(subscriber)) {
            eventBus.register(subscriber);
        }
    }

    /**
     * 解除注册 EventBus
     *
     * @param subscriber
     */
    public static void unregister(Object subscriber) {
        EventBus eventBus = EventBus.getDefault();
        if (eventBus.isRegistered(subscriber)) {
            eventBus.unregister(subscriber);
        }
    }

    /**
     * 发送事件消息
     *
     * @param event
     */
    public static void post(EventMessage event) {
        EventBus.getDefault().post(event);
    }

    /**
     * 发送粘性事件消息
     *
     * @param event
     */
    public static void postSticky(EventMessage event) {
        EventBus.getDefault().postSticky(event);
    }
}
