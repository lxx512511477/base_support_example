package com.sx.base_support;

import android.app.Application;
import android.content.Context;

import com.alibaba.android.arouter.launcher.ARouter;
import com.orhanobut.logger.AndroidLogAdapter;
import com.orhanobut.logger.FormatStrategy;
import com.orhanobut.logger.Logger;
import com.orhanobut.logger.PrettyFormatStrategy;
import com.simple.spiderman.SpiderMan;
import com.sx.base_support.logger.LogCatStrategy;
import com.sx.base_support.utils.Utils;

import cc.rome753.activitytask.ActivityTask;


/**
 * 预备管理类
 * <p>
 * 作用于公共库的初始化
 * <p>
 * 示例
 */
public class BaseManager {

    private boolean hasActivityTask = false;
    private boolean hasSpiderMan = false;
    private Context context;

    public static class Builder {
        private BaseManager baseManager;
        //必选字段

        //通过builder模式，给对象添加属性；
        public Builder(Context context) {
            baseManager = new BaseManager(context);
        }

        public Builder hasActivityTask(boolean b) {
            baseManager.hasActivityTask = b;
            return this;
        }

        public Builder hasSpiderMan(boolean b) {
            baseManager.hasSpiderMan = b;
            return this;
        }

        //返回外部对象
        public BaseManager build() {
            return baseManager.init();
        }
    }

    private BaseManager(Context context) {
        this.context = context;
    }

    private BaseManager init() {
        if (hasSpiderMan) {
            //放在其他库初始化前
            SpiderMan.init(context);
        }

        if (BuildConfig.DEBUG) {           // 这两行必须写在init之前，否则这些配置在init过程中将无效
            ARouter.openLog();     // 打印日志
            ARouter.openDebug();   // 开启调试模式(如果在InstantRun模式下运行，必须开启调试模式！线上版本需要关闭,否则有安全风险)
        }
        ARouter.init(Utils.getApp()); // 尽可能早，推荐在Application中初始化


        if (hasActivityTask) {
            ActivityTask.init((Application) context, BuildConfig.DEBUG);
        }

        //logger 日志输出错位修正
        FormatStrategy formatStrategy = PrettyFormatStrategy.newBuilder()
                .showThreadInfo(false)// (Optional) Whether to show thread info or not. Default true
                .logStrategy(new LogCatStrategy())// (Optional) Changes the log strategy to print out. Default LogCat
                .methodCount(0)// (Optional) How many method line to show. Default 2
                .methodOffset(7) // (Optional) Hides internal method calls up to offset. Default 5
                .tag("tag")// (Optional) Global tag for every log. Default PRETTY_LOGGER
                .build();
        Logger.addLogAdapter(new AndroidLogAdapter(formatStrategy) {
            @Override
            public boolean isLoggable(int priority, String tag) {
                return BuildConfig.DEBUG;
            }
        });
        return this;
    }
}
