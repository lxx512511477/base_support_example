package com.sx.base_support.app;

import android.app.Application;
import android.content.Context;

import com.orhanobut.logger.Logger;
import com.sx.base_support.BaseManager;
import com.sx.base_support.BuildConfig;
import com.sx.base_support.utils.ToastUtils;

import me.yokeyword.fragmentation.Fragmentation;

public class App extends Application {

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        new BaseManager.Builder(this)
                .hasActivityTask(true)
                .hasSpiderMan(true)
                .build();
        Fragmentation.builder()
                // 设置 栈视图 模式为 （默认）悬浮球模式   SHAKE: 摇一摇唤出  NONE：隐藏， 仅在Debug环境生效
//                .stackViewMode(Fragmentation.BUBBLE)
                .debug(BuildConfig.DEBUG) // 实际场景建议.debug(BuildConfig.DEBUG)
                .install();

        Logger.e("App------init");
    }
}
