package com.sx.base_support_ui.widget.segmentTabLayout;

public interface OnTabSelectListener {
    void onTabSelect(int position);
    void onTabReselect(int position);
}