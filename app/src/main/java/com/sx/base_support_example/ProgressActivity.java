package com.sx.base_support_example;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.daimajia.numberprogressbar.NumberProgressBar;
import com.sx.base_support_ui.widget.loading.LVCircularZoom;

@Route(path = Constants.TEST_PROGRESS)
public class ProgressActivity extends AppCompatActivity {

    private LVCircularZoom lv_circularZoom;
    private NumberProgressBar number_progress_bar;
private int progress=10;

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what==100){
                if (progress<100){
                    number_progress_bar.setProgress(progress);
                    progress++;
                    handler.sendEmptyMessage(100);
                }

            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_progress);

        lv_circularZoom = findViewById(R.id.lv_circularZoom);
        number_progress_bar = findViewById(R.id.number_progress_bar);
        lv_circularZoom.setColor(Color.BLACK);
        lv_circularZoom.startAnim();

        number_progress_bar.setProgress(progress);
        handler.sendEmptyMessageDelayed(100,3000);
    }
}
