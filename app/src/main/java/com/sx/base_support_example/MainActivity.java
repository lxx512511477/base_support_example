package com.sx.base_support_example;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.alibaba.android.arouter.launcher.ARouter;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.sx.base_support_ui.baseUI.BaseSupportActivity;
import com.sx.base_support_ui.widget.DividerItemDecoration;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends BaseSupportActivity {

    private RecyclerView recyclerView;

    private BaseQuickAdapter<String, BaseViewHolder> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.rec);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.HORIZONTAL_LIST));

        adapter = new BaseQuickAdapter<String, BaseViewHolder>(R.layout.item_main) {
            @Override
            protected void convert(BaseViewHolder helper, String item) {
                helper.setText(R.id.tv_title, item);
            }
        };
        adapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                switch (position) {
                    case 0:
                        ARouter.getInstance().build(Constants.TEST_TITLE).navigation();
                        break;
                    case 1:
                        ARouter.getInstance().build(Constants.TEST_PROGRESS).navigation();
                        break;
                    case 2:

                        break;
                    case 3:
                        ARouter.getInstance().build(Constants.TEST_ANIMA).navigation();
                        break;
                    case 4:
                        ARouter.getInstance().build(Constants.TEST_VIEW).navigation();
                        break;
                    case 5:
                        ARouter.getInstance().build(Constants.TEST_DIALOG).navigation();
                        break;
                    case 6:
                        break;
                    case 7:
                        ARouter.getInstance().build(Constants.TEST_SPAN).navigation();
                        break;

                }
            }
        });
        recyclerView.setAdapter(adapter);
        initData();
    }

    private void initData() {
        List<String> list = new ArrayList<>();
        list.add("标题titlebar");
        list.add("进度条");//数字进度条、loading进度条
        list.add("圆角view");
        list.add("动画view");//文本公告滚动，数字滚动,水波纹
        list.add("滑动选择按钮");
        list.add("自定义弹窗");//确认弹窗、加载弹窗、底部弹窗、自定义弹窗
        list.add("倒计时");
        list.add("SpanUtils处理");
        list.add("带删除输入框");

        adapter.setNewData(list);
    }
}
