package com.sx.base_support_example;

import android.os.Bundle;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.sx.base_support_example.R;
import com.sx.base_support_ui.baseUI.BaseSupportActivity;

@Route(path = Constants.TEST_TITLE)
public class TitleActivity extends BaseSupportActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_title);
    }
}
