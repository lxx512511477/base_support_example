package com.sx.base_support_example;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.skyfishjy.library.RippleBackground;
import com.superluo.textbannerlibrary.TextBannerView;
import com.yy.mobile.rollingtextview.RollingTextView;

import java.util.ArrayList;
import java.util.List;


@Route(path = Constants.TEST_ANIMA)
public class AnimaActivity extends AppCompatActivity {
    private TextBannerView tv_banner;
    private RollingTextView tv_rolling;
    private RippleBackground rippleBackground;

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == 101) {
                num++;
                tv_rolling.setText(num + "");
                handler.sendEmptyMessageDelayed(101, 1000);
            }
        }
    };
    private int num = 10001;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_anima);

        tv_banner = findViewById(R.id.tv_banner);
        tv_rolling = findViewById(R.id.tv_rolling);
        rippleBackground = findViewById(R.id.ripple_bg);

        initTextBanner();
        initRollingTextView();


        rippleBackground.startRippleAnimation();
    }

    private void initTextBanner() {
        List<String> list = new ArrayList<>();
        list.add("文本1");
        list.add("文本2");
        list.add("文本3");
        list.add("文本4");
        list.add("文本5");
        tv_banner.setDatas(list);
        tv_banner.startViewAnimator();
    }

    private void initRollingTextView() {
        handler.sendEmptyMessageDelayed(101, 1000);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

        rippleBackground.stopRippleAnimation();
        if (handler != null) {
            handler.removeCallbacksAndMessages(null);
        }
    }
}
