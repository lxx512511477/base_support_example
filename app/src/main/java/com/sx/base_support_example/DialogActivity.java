package com.sx.base_support_example;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.TextView;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.coder.zzq.smartshow.dialog.DialogBtnClickListener;
import com.coder.zzq.smartshow.dialog.EnsureDialog;
import com.coder.zzq.smartshow.dialog.LoadingDialog;
import com.coder.zzq.smartshow.dialog.SmartDialog;
import com.sx.base_support.utils.ToastUtils;
import com.sx.base_support_ui.baseUI.BaseSupportActivity;

@Route(path = Constants.TEST_DIALOG)
public class DialogActivity extends BaseSupportActivity implements View.OnClickListener {

    private TextView tv_1, tv_2;
    private LoadingDialog loadingDialog;
    private Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dialog);

        tv_1 = findViewById(R.id.tv_1);
        tv_2 = findViewById(R.id.tv_2);
        tv_1.setOnClickListener(this);
        tv_2.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_1:
                new EnsureDialog()
                        .message("默认msg")
                        .title("默认标题")
                        .cancelBtn("取消按钮", new DialogBtnClickListener() {
                            @Override
                            public void onBtnClick(SmartDialog smartDialog, int i, Object o) {
                                ToastUtils.showShort("取消回调");
                            }
                        })
                        .confirmBtn("确认按钮", new DialogBtnClickListener() {
                            @Override
                            public void onBtnClick(SmartDialog smartDialog, int i, Object o) {
                                ToastUtils.showShort("确认回调");
                            }
                        })
                        .showInActivity(this);
                break;
            case R.id.tv_2:
                loadingDialog = new LoadingDialog()
                        .withMsg(true)
                        .message("正在加载");
                loadingDialog.showInActivity(this);
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        loadingDialog.dismiss();
                    }
                }, 3000);
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (handler != null) {
            handler.removeCallbacksAndMessages(null);
        }
    }
}
