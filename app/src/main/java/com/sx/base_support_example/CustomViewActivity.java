package com.sx.base_support_example;

import android.os.Bundle;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.sx.base_support.utils.ToastUtils;
import com.sx.base_support_ui.baseUI.BaseSupportActivity;
import com.sx.base_support_ui.widget.segmentTabLayout.OnTabSelectListener;
import com.sx.base_support_ui.widget.segmentTabLayout.SegmentTabLayout;

@Route(path = Constants.TEST_VIEW)
public class CustomViewActivity extends BaseSupportActivity {

    private String[] strings = {"标签A", "标签B"};
    private SegmentTabLayout stab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_view);

        stab = findViewById(R.id.stab);
        stab.setTabData(strings);
        stab.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelect(int position) {
                ToastUtils.showShort("选择了第"+position+"个");
            }

            @Override
            public void onTabReselect(int position) {
            }
        });
    }
}
