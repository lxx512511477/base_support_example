package com.sx.base_support_example;

public class Constants {
    public static final String TEST_TITLE = "/test/TitleActivity";
    public static final String TEST_PROGRESS = "/test/ProgressActivity";
    public static final String TEST_VIEW = "/test/CustomViewActivity";
    public static final String TEST_DIALOG = "/test/DialogActivity";
    public static final String TEST_ANIMA = "/test/AnimaActivity";
    public static final String TEST_SPAN = "/test/SpanActivity";
}
