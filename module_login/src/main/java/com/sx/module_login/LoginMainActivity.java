package com.sx.module_login;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.sx.base_support.utils.ToastUtils;

public class LoginMainActivity extends AppCompatActivity {
    private Button bt_test;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_main);

        bt_test = findViewById(R.id.bt_test);

        bt_test.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ToastUtils.showShort("点到我了!");
            }
        });
    }
}
